Perspective Transform
=====================

This demonstrates a *working* 4-point perspective transform of a webcam video using HTML5 canvas & three.js.


Intro
=====

Here's an example of what this code does:

![alt text](https://bitbucket.org/mattwilson1024/perspective-transform/raw/ac2da9a1287a22f1b38b9da48d4c1166e9feeda5/example.png "Example")

The goal here is to have a web page which works as follows:

1. The user points their webcam towards a TV, so that it is somewhere in the frame (but potentially at any angle)
2. Using HTML5 video & canvas, the webcam is captured and previewed on the web page
3. The user is able to define (by clicking on the preview) where the 4 corners of the TV screen are (4 pairs of x/y coordinates)
4. ** The video is warped (using some kind of perspective transform) so that the canvas _only_ shows the part of the image for their actual TV screen (not the whole webcam view) **
5. Some processing is then performed on the image (for example, identifying most prominent colours). The detail of this is outside of the scope of this question, other than the fact that I will want to be able to access the content/pixels of an HTML5 canvas at the end.


How to Run (and see the problem)
================================

1. Install dependencies - `npm install` or `yarn install`
2. Start the app - `npm start` or `yarn start`
3. Browse to http://localhost:1234 (browser needs to support WebGL - I suggest Chrome)
4. You should see a canvas showing your webcam video with a red dot at the origin (in the centre)
5. Click four points to define an area in the image. Important: click in the following order: bottom left, bottom right, top right, top left.
6. After the 4th point, the transform is applied.



StackOverflow Question
======================

Note: Before I got this working correctly, I asked a question on StackOverflow asking for help because the transform was not working correctly:
[https://stackoverflow.com/questions/53997984/how-to-implement-4-point-perspective-transform-using-html5-canvas-three-js](https://stackoverflow.com/questions/53997984/how-to-implement-4-point-perspective-transform-using-html5-canvas-three-js)

The broken version of the code which accompanies the StackOverflow question can be found in the [wrong-transform branch](https://bitbucket.org/mattwilson1024/perspective-transform/src/wrong-transform/).

This `master` branch contains the fixed version.


Resources
=========

StackOverflow answer showing the maths needed to compute a projective transform:
  https://stackoverflow.com/questions/14244032/redraw-image-from-3d-perspective-to-2d/14244616#14244616

Math StackExchange answer with information about the transform:
  https://math.stackexchange.com/a/339033

Useful answer on resources to learn linear algebra (points, vectors, matrices):
  https://stackoverflow.com/a/10671420/1145963

Udacity course on 3d Graphics with Three.js:
  https://classroom.udacity.com/courses/cs291

How to Fit Camera to Object:
  https://stackoverflow.com/questions/14614252/how-to-fit-camera-to-object

Threejs docs:
  https://threejs.org/docs/#manual/en/introduction/Creating-a-scene

perspective-transform lib:
  https://github.com/jlouthan/perspective-transform
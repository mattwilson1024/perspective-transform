/** Simple interface that can be used whenever we need to refer to a width and height */
export interface IDimensions {
  width: number;
  height: number;
}
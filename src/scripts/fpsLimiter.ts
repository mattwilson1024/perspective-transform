/**
 * A utility to help limit the number of times we process frames of a video, in order to improve performance
 * and avoid overly heavy use of system resources. The class keeps track of how long has elapsed between calls to it's
 * `newFrame` method.
 * 
 * To use, create an instance of `FpsLimiter` and provide the maximum number of frames that you want to process per second.
 * In the main loop, call `newFrame()` and only perform a new render if it returns true.
 * 
 * Based on https://stackoverflow.com/questions/19764018/controlling-fps-with-requestanimationframe
 * TODO: "not being a multiple of RAF's interval (16.7ms)"
 */
export class FpsLimiter {
  private fpsInterval: number;
  private lastTime: number|null = null;

  constructor(private maxFps: number) {
    this.fpsInterval = 1000/maxFps;
  }

  public newFrame(): boolean {
    const now = performance.now();

    if (!this.lastTime) {
      this.lastTime = now;
      return true;
    }

    const elapsed = now - this.lastTime;
    if (elapsed > this.fpsInterval) {
      this.lastTime = now;
      return true;
    } else {
      return false;
    }
  }
}
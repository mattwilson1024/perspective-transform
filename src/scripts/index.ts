import { ThreeJsScene } from "./threeJsScene";
import { Webcam } from "./webcam";
import { IDimensions } from "./dimensions";
import { RenderLoop } from "./renderLoop";

async function main() {
  // Grab the DOM nodes for the <video> and <canvas> elements
  const videoElement = document.getElementById('myVideo') as HTMLVideoElement;
  const canvasElement = document.getElementById('myCanvas') as HTMLCanvasElement;

  // Capture the webcam
  const webcam = new Webcam(videoElement, canvasElement);
  const webcamDimensions = await webcam.startCapturingWebcam();
  console.log(`Video is ${webcamDimensions.width}x${webcamDimensions.height}px`);

  // Prepare the output canvas
  const canvasDimensions: IDimensions = {
    width: webcamDimensions.width / 2, // TODO: Work out a suitable size based on aspect ratio etc.
    height: webcamDimensions.height / 2
  }
  canvasElement.width = canvasDimensions.width;
  canvasElement.height = canvasDimensions.height;

  // Create a three.js scene which will be used to warp the video and render it into the canvas
  const threeJsScene = new ThreeJsScene(videoElement, canvasElement, canvasDimensions);
  threeJsScene.setupScene();

  // Render the video up to a maximum of 25fps
  const renderLoop = new RenderLoop(25);
  renderLoop.runLoop(() => threeJsScene.renderFrame());
}

main();
import { FpsLimiter } from "./fpsLimiter";

/**
 * The `RenderLoop` is a utility class which deals with the timing of frames for the video and processing.
 * 
 * After instantiating and specifying the maximum frames per second, call `runLoop` to start the ball rolling.
 * `runLoop` should be provided a callback function which defines the action to perform each time a new frame is ready to be processed (for example rendering the video into a canvas).
 * 
 * The scheduling will be handled using a combination of `requestAnimationFrame` and the `FpsLimiter`. 
 * This avoids rendering too frequently which would reduce performance and place heavy demand on system resources.
 */
export class RenderLoop {
  private fpsLimiter: FpsLimiter;
  private renderFunction: Function;

  constructor(maxFps: number) {
    this.fpsLimiter = new FpsLimiter(maxFps);
  }
  
  public runLoop(renderFunction: Function) {
    this.renderFunction = renderFunction;
    this.renderFrame();
  }

  private renderFrame() {
    requestAnimationFrame(() => this.renderFrame());
  
    const shouldDraw = this.fpsLimiter.newFrame();
    if (shouldDraw) {
      this.renderFunction();
    }
  }
}
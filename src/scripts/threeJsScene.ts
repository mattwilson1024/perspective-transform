import { Scene, PerspectiveCamera, BoxGeometry, MeshBasicMaterial, Mesh, VideoTexture, LinearFilter, RGBFormat, WebGLRenderer, Matrix4, Raycaster, Object3D, Vector3 } from 'three';
import perspectiveTransform from 'perspective-transform';
import { IDimensions } from './dimensions';

export class ThreeJsScene {
  private scene: Scene;
  private camera: PerspectiveCamera;
  private raycaster: Raycaster;
  private renderer: WebGLRenderer;
  private videoTexture: VideoTexture;
  private objects: Object3D;
  private objectsName = 'ALL_OBJECTS';
  
  private videoMesh: Mesh;
  private originMesh: Mesh;
  private cornerMeshes: Mesh[] = [];

  private corners = [[0, 0], [0, 0], [0, 0], [0, 0]];
  private settingPointIndex = 0;
  private transformEnabled = false;

  constructor(private videoElement: HTMLVideoElement, 
              private canvasElement: HTMLCanvasElement,
              private canvasDimensions: IDimensions) {
  }

  setupScene() {
    const aspectRatio = this.canvasDimensions.width / this.canvasDimensions.height;
    const fov = 75;
    const canvasShortestEdge = Math.min(this.canvasDimensions.width, this.canvasDimensions.height);
    const cameraDistance = canvasShortestEdge / 2 / Math.tan(Math.PI * fov / 360);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(fov, aspectRatio, 0.1, 1000); 
    this.camera.position.z = cameraDistance;
    
    this.renderer = new WebGLRenderer({ canvas: this.canvasElement });
    this.renderer.setSize(this.canvasDimensions.width, this.canvasDimensions.height);
    
    // Setup a raycaster which can work out the position in the image that has been clicked in 3D space
    this.raycaster = new Raycaster();
    this.canvasElement.onclick = (event) => this.onCanvasClicked(event);

    // Create a containing group which will include all objects
    this.objects = new Object3D();
    this.objects.name = this.objectsName;

    // Create a mesh for the video feed
    this.videoTexture = new VideoTexture(this.videoElement);
    this.videoTexture.minFilter = LinearFilter;
    this.videoTexture.magFilter = LinearFilter;
    this.videoTexture.format = RGBFormat;

    this.videoMesh = new Mesh(
      new BoxGeometry(this.canvasDimensions.width, this.canvasDimensions.height, 1),
      new MeshBasicMaterial({ map: this.videoTexture })
    );
    this.objects.add(this.videoMesh);

    // Create a mesh to show the origin location
    this.originMesh = new Mesh(
      new BoxGeometry(5, 5, 0),
      new MeshBasicMaterial({ color: '#ff0000' })
    );
    this.objects.add(this.originMesh);

    // Create four meshes to show the corner points
    for (let i = 0; i < 4; i++) {
      const cornerMesh = new Mesh(
        new BoxGeometry(5, 5, 0),
        new MeshBasicMaterial({ color: '#00ff00' })
      );
      this.cornerMeshes.push(cornerMesh);
      this.objects.add(cornerMesh);
    }
    this.repositionCornerMeshes();

    // Finally, add all the objects to the scene
    this.scene.add(this.objects);
  }

  public renderFrame() {
    this.renderer.render(this.scene, this.camera);
  }

  onCanvasClicked(event: MouseEvent) {
    // Don't bubble the click event
    event.preventDefault();

    // Get the x/y position of the click in the range -1 to 1
    const canvasPosition = this.canvasElement.getBoundingClientRect();
    const x = ((event.clientX - canvasPosition.left) / this.canvasDimensions.width) * 2 - 1;
    const y = -((event.clientY - canvasPosition.top) / this.canvasDimensions.height) * 2 + 1;

    // Use the raycaster to figure out the x/y coordinates of the click in 3D space
    this.raycaster.setFromCamera({ x, y }, this.camera);
    const intersects = this.raycaster.intersectObjects(this.scene.getObjectByName(this.objectsName).children, true);
    if (intersects.length > 0) {
      const intersectionCoordinates: Vector3 = intersects[0].point;
      this.respondToClick(intersectionCoordinates);
    }
  };

  private respondToClick(clickedCoordinates: Vector3) {
    this.corners[this.settingPointIndex] = [clickedCoordinates.x, clickedCoordinates.y];
    this.settingPointIndex++;
    this.repositionCornerMeshes();

    console.log(this.corners);

    if (this.settingPointIndex > 3) {
      this.settingPointIndex = 0;
      this.transformEnabled = true;
      this.applyTransform();
      this.repositionCornerMeshes();
    }
  }

  private repositionCornerMeshes() {
    for (let i = 0; i < 4; i++) {
      const [x, y] = this.corners[i];
      this.cornerMeshes[i].position.x = x;
      this.cornerMeshes[i].position.y = y;
      this.cornerMeshes[i].visible = !this.transformEnabled && this.settingPointIndex > i;
    }
  }

  private applyTransform() {
    // Source points come from the clicked corner points - flatten `this.corners` (an array of dimensions) into a single array of alternating x/y coordinates
    // e.g. `[[0, 1], [2, 3], [4, 5], [6, 7]]` becomes `[0, 1, 2, 3, 4, 5, 6, 7]`
    const srcPts = this.corners.reduce((acc, point) => {
      const [x, y] = point;
      return [...acc, x, y];
    }, []);

    // Destination points - this is the four corners of the visible viewport (bottom left, bottom right, top right, top left)
    // These are half the width & height because the origin is at the centre
    const w = this.canvasDimensions.width/2;
    const h = this.canvasDimensions.height/2;
    const destPts = [
      -w, -h, // bottom left
      w, -h, // bottom right
      w, h, // top right
      -w, h, // top left
    ];

    // Debug
    console.log('src', srcPts);
    console.log('destPts', destPts);

    // Get the coefficience for a perspective transform mapping from the source (TV screen corners) to the destination (entire viewport)
    const perspT = perspectiveTransform(srcPts, destPts);
    const [a1, a2, a3, b1, b2, b3, c1, c2, c3] = perspT.coeffs;

    // Create a 4x4 transformation matrix.
    // Transform x and y coordinate, but leave the z coordinates of the homogenous coordinate vectors alone
    // For info, see https://math.stackexchange.com/a/339033
    const transformMatrix = new Matrix4();
    transformMatrix.set(a1, a2, 0, a3, 
                        b1, b2, 0, b3, 
                        0,  0,  0, 1, 
                        c1, c2, 0, c3);

    // Apply the tranform to the video mesh
    this.videoMesh.matrix = transformMatrix;
    this.videoMesh.matrixAutoUpdate = false;
  }
}
import { fromEvent } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { IDimensions } from './dimensions';

export class Webcam {

  constructor(private videoElement: HTMLVideoElement, 
              private canvasElement: HTMLCanvasElement) {
  }

  /**
   * Capture the user's webcam and set it as the source for the <video> element.
   * Returns a promise when the video is ready - this resolves with an object that tells you the video's width and height.
   */
  public async startCapturingWebcam(): Promise<IDimensions> {
    if (navigator.mediaDevices.getUserMedia) {
      const stream = await navigator.mediaDevices.getUserMedia({ video: { width: 1280, height: 720 } }).catch(err => console.error(err));
      if (stream) {
        const videoCanPlayPromise = fromEvent(this.videoElement, 'playing').pipe(
          take(1),
          map((event: Event) => {
            return {
              width: this.videoElement.videoWidth,
              height: this.videoElement.videoHeight
            } as IDimensions;
          })
        ).toPromise();

        this.videoElement.srcObject = stream;
        this.videoElement.play();
        return videoCanPlayPromise;
      }
    }
  }

}